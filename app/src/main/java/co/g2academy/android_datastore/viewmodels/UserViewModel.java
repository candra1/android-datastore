package co.g2academy.android_datastore.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import co.g2academy.android_datastore.model.LoginRequest;
import co.g2academy.android_datastore.model.User;
import co.g2academy.android_datastore.model.UserResponse;
import co.g2academy.android_datastore.networking.UserRepository;


public class UserViewModel extends ViewModel {
    private UserRepository usersRepository;
    private MutableLiveData<UserResponse> mutableLiveData;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        usersRepository = UserRepository.getInstance();
        
    }


    public LiveData<UserResponse> postLogin(LoginRequest loginRequest) {
        if (mutableLiveData == null) {
            usersRepository = UserRepository.getInstance();
        }
        mutableLiveData = usersRepository.postLogin(loginRequest);

        return mutableLiveData;
    }


}

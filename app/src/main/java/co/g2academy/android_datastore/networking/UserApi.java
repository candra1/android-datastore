package co.g2academy.android_datastore.networking;

import co.g2academy.android_datastore.model.LoginRequest;
import co.g2academy.android_datastore.model.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi {
    @POST("user/login")
    Call<UserResponse> postLogin(@Body LoginRequest body);
}

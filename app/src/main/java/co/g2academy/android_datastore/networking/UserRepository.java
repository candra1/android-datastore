package co.g2academy.android_datastore.networking;

import androidx.lifecycle.MutableLiveData;

import co.g2academy.android_datastore.model.LoginRequest;
import co.g2academy.android_datastore.model.UserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {
    private static UserRepository usersRepository;

    public static UserRepository getInstance(){
        if (usersRepository == null){
            usersRepository = new UserRepository();
        }
        return usersRepository;
    }

    private UserApi userApi;

    public UserRepository(){
        userApi = RetrofitService.cteateService(UserApi.class);
    }

    public MutableLiveData<UserResponse> postLogin(LoginRequest loginRequest){
        MutableLiveData<UserResponse> userData = new MutableLiveData<>();
        userApi.postLogin(loginRequest).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call,
                                   Response<UserResponse> response) {
                if (response.isSuccessful()){
                    userData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                userData.setValue(null);
            }
        });
        return userData;
    }

}

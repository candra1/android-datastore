package co.g2academy.android_datastore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.prefs.Preferences;

import co.g2academy.android_datastore.model.LoginRequest;
import co.g2academy.android_datastore.model.User;
import co.g2academy.android_datastore.viewmodels.UserViewModel;

public class MainActivity extends AppCompatActivity {

    Button loginButton, registerButton;
    EditText emailEditText, passwordEditText;
    UserViewModel userViewModel;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        onClickGroup();
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.init();
    }
    void findViewById(){
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
    }
    void onClickGroup(){
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin(emailEditText.getText().toString(), passwordEditText.getText().toString());
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                intent.putExtra("name","Candra S");
//                startActivity(intent);
                startActivityForResult(intent,2212);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2212) {
            if (resultCode == Activity.RESULT_OK) {
                String selectedImage = data.getExtras().getString("streetkey");
                Toast.makeText(MainActivity.this,selectedImage,Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    private void doLogin(String email, String password ){

        userViewModel.postLogin(new LoginRequest(email,password)).observe(this, userResponse -> {
            user = userResponse.getData();
            //SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences sharedPref = getSharedPreferences("co.g2academy.android_datastore", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("co.g2academy.android_datastore.token", user.getToken());
            editor.apply();

            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);

        });
    }
}